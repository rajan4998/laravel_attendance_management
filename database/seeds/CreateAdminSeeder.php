<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'is_admin' => 1,
            'validated' => 1,
            'password' => Hash::make('Admin@123'),
        ]);
    }
}
