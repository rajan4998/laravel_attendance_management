<?php

namespace App\Http\Middleware;

use Closure;

class CheckValidatedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->validated==0){
            return redirect('verify_email');
        }
        return $next($request);
    }
}
